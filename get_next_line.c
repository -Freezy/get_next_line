/*
** get_next_line.c for get next line in /home/doghri_f/rendu/CPE_2013_getnextline
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Sat Nov 23 18:36:40 2013 doghri_f
** Last update Sun Apr 13 15:32:05 2014 doghri_f
*/

#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      i++;
    }
  return (dest);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    i++;
  return (i);
}

char	*my_realloc(char *str, int length)
{
  char	*copy;

  copy = malloc((length * (my_strlen(str) + 1)) * sizeof(char));
  my_strcpy(copy, str);
  free(str);
  return (copy);
}

char	*get_next_line(const int fd)
{
  static int	i = 0;
  static char	buffer[SIZE_BUFFER + 1];
  char		*str;
  int		recup;
  int		y;

  y = 0;
  recup = 1;
  str = malloc(1 * sizeof(char));
  if (str == NULL)
    return (0);
  while (recup != 0 && buffer[i] != '\n')
    {
      if (buffer[i] == '\0')
	{
	  i = 0;
	  recup = read(fd, buffer, SIZE_BUFFER);
	  str = my_realloc(str, recup);
	  buffer[recup] = '\0';
	}
      while (buffer[i] != '\n' && buffer[i] != '\0')
	str[y++] = buffer[i++];
    }
  if (buffer[i] == '\n')
    i++;
  str[y] = '\0';
  return (str);
}

int	main()
{
  int  c;
  char *d;
  c = 0;
  while (c < 5)
    {
      d = get_next_line(0);
      c++;
      printf("%s\n", d);
    }
}
