/*
** get_next_line.h for get next line in /home/doghri_f/rendu/CPE_2013_getnextline
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Sat Nov 23 18:42:18 2013 doghri_f
** Last update Sun Apr 13 15:31:37 2014 doghri_f
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define SIZE_BUFFER 5

char	*get_next_line(const int fd);

int	my_strlen(char *str);

char	*my_strcpy(char *dest, char *src);

#endif /* !GET_NEXT_LINE_H_ */

